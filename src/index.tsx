import './styles/global.css'
import 'tailwindcss/tailwind.css'
import { createRoot } from 'react-dom/client'
import { ThemeProvider } from '@material-tailwind/react'
import { RouterProvider } from 'react-router-dom';
import { mainRouter } from 'routes/root';

const container = document.getElementById('root') as HTMLDivElement
const root = createRoot(container)

root.render(
    <ThemeProvider>
        <div className={`font-['Poppins']`}>
            <div className="container max-w-md lg:max-w-sm relative overflow-hidden bg-gray-100">
                <div className="h-screen p-8 sm:pt-18">
                    <RouterProvider router={mainRouter} />
                </div>
            </div>
        </div>
    </ThemeProvider>
)
