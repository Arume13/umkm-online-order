import { createBrowserRouter } from "react-router-dom";
import App from "components/App";
import ErrorPage from "error-page";
import { StartScreen } from "pages/start_screen";
import { StartUser } from "pages/client/start";
import { StartMerchant } from "pages/merchant/start";
import { SignUpUser } from "pages/client/sign_up";
import { SignUpMerchant } from "pages/merchant/sign_up";
import { SignInMerchant } from "pages/merchant/Sign_in";
import { SignInUser } from "pages/client/sign_in";
import { SuccessUser } from "pages/client/success";
import { SuccessMerchant } from "pages/merchant/Success";
import { WaitingMerchant } from "pages/merchant/waiting";
import { ProfileUser } from "pages/client/profile";
import { ProfileMerchant } from "pages/merchant/profile";
import { HomeUser } from "pages/client/home";
import { HomeMerchant } from "pages/merchant/home";

export const mainRouter = createBrowserRouter([
    {
      path: "/",
      element: <StartScreen />,
      errorElement: <ErrorPage />,
    },
    {
      path: "/client",
      element: <StartUser />,
      errorElement: <ErrorPage />,
    },
    {
      path: "/merchant",
      element: <StartMerchant />,
      errorElement: <ErrorPage />,
    },


    // User Routes
    {
      path: "/client/sign-up",
      element: <SignUpUser />,
      errorElement: <ErrorPage />,
    },
    {
      path: "/client/sign-in",
      element: <SignInUser />,
      errorElement: <ErrorPage />,
    },
    {
      path: "/client/success",
      element: <SuccessUser />,
      errorElement: <ErrorPage />,
    },
    {
      path: "/client/profile",
      element: <ProfileUser />,
      errorElement: <ErrorPage />,
    },
    {
      path: "/client/home",
      element: <HomeUser />,
      errorElement: <ErrorPage />,
    },


    // Merchant Routes
    {
      path: "/merchant/sign-up",
      element: <SignUpMerchant />,
      errorElement: <ErrorPage />,
    },
    {
      path: "/merchant/sign-in",
      element: <SignInMerchant />,
      errorElement: <ErrorPage />,
    },
    {
      path: "/merchant/success",
      element: <SuccessMerchant />,
      errorElement: <ErrorPage />,
    },
    {
      path: "/merchant/waiting",
      element: <WaitingMerchant />,
      errorElement: <ErrorPage />,
    },
    {
      path: "/merchant/profile",
      element: <ProfileMerchant />,
      errorElement: <ErrorPage />,
    },
    {
      path: "/merchant/home",
      element: <HomeMerchant />,
      errorElement: <ErrorPage />,
    },
  ]);