import { Cards } from "./component/Cards"
import { Footer } from "./component/Footer"
import {NavBar} from "./component/Navbar"
import { Searchbar } from "./component/Searchbar"

export const HomeUser = () => {
    return (
        <>
        <NavBar />
        <Searchbar/>
        <div className="grid grid-cols-2 gap-4">
        <Cards/>
        <Cards/>
        <Cards/>
        <Cards/>
        </div>
        <Footer/>
        </>
    )
}
