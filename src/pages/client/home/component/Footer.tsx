import { IconButton, } from '@material-tailwind/react'
import { FiHome,FiMessageCircle,FiFileText } from "react-icons/fi";
import { useNavigate } from "react-router-dom";

export function Footer() {
    const navigate = useNavigate();
    return (
        <>
            <div className='w-[439px] h-[150px] bg-[#5DB329] mt-40 -ml-10'>
                <div className='content-center grid grid-cols-3 pt-6'>
                    <div className='m-auto'onClick={() => navigate("/client/sign-up")}>
                    <FiHome className='text-white size-12 ml-10 ' />
                    <p className='text-white ml-8'>Beranda</p>
                    </div>
                    <div className='m-auto' onClick={() => navigate("/client/sign-up")}>
                    <FiMessageCircle className='text-white size-12 m-auto' />
                    <p className='text-white'>Pesan</p>
                    </div>
                    <div className='m-auto' onClick={() => navigate("/client/sign-up")}>
                    <FiFileText className='text-white size-12 m-auto mr-10' />
                    <p className='text-white mr-8'>Riwayat</p>
                    </div>
                </div>
            </div>
        </>
    );
}