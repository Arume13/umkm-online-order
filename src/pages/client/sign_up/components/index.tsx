import { Button, ButtonGroup, Input } from '@material-tailwind/react'

export const Index = () => {
    return (
        <>
            <svg width="439" height="518" viewBox="0 0 439 568" className='-mt-24 -ml-8 lg:-ml-12' fill="none" xmlns="http://www.w3.org/2000/svg">
                <g filter="url(#filter0_f_87_653)">
                    <ellipse cx="219" cy="259" rx="158" ry="159" fill="url(#paint0_linear_87_653)" />
                </g>
                <defs>
                    <filter id="filter0_f_87_653" x="-39" y="0" width="516" height="518" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                        <feFlood flood-opacity="0" result="BackgroundImageFix" />
                        <feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
                        <feGaussianBlur stdDeviation="50" result="effect1_foregroundBlur_87_653" />
                    </filter>
                    <linearGradient id="paint0_linear_87_653" x1="35.2391" y1="392.94" x2="424.216" y2="34.4785" gradientUnits="userSpaceOnUse">
                        <stop stop-color="#5DB329" />
                        <stop offset="0.825" stop-color="#D9D9D9" stop-opacity="0" />
                    </linearGradient>
                </defs>
            </svg>


            <div className="flex justify-center items-center ml-14 w-[14rem] -mt-[24rem] lg:w-[14rem] lg:-mt-[22rem]">
                <img src="https://i.ibb.co/DzVDT8N/image.png" className='' alt="" />
            </div>


            <p className="text-lg text-center text-gray-900 mb-5 mt-48 lg:mt-32">
                Ayo daftar untuk memulai!
            </p>

            <div className="mb-4">
                <Input label="Email" className='mb-4' icon={<i className="fas fa-heart" />} />
            </div>
            <div className="mb-4">
                <Input label="Nama Pengguna" className='mb-4' icon={<i className="fas fa-heart" />} />
            </div>
            <div className="mb-4">
                <Input label="Kata Sandi" className='mb-4' icon={<i className="fas fa-heart" />} />
            </div>


            <div className="flex mt-8 justify-center items-center">
                <div className="block w-[16rem]">
                    <Button size="lg" color='green' className='rounded-full' fullWidth>Daftar</Button>
                </div>
            </div>
        </>
    )
}
