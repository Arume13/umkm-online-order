import {Input } from '@material-tailwind/react'
import { FiSearch } from "react-icons/fi";

export const Searchbar = () => {
    return (
        <>
            <div className='row-auto mt-12'>
                <h1 className='font-semibold text-2xl'>Hallo Pedagang!</h1>
                <p className='text-[#202C59]'>Mari kita cari pelanggan anda</p>
            </div>
            <div className='mt-10'>
                <Input type="search" placeholder="Search" 
                    className="w-80 rounded-[20px] border border-blue-gray-200 border-t-transparent !border-t-blue-gray-300 bg-transparent px-3 py-2.5 pl-9 font-sans text-sm font-normal text-blue-gray-700 outline outline-0 transition-all placeholder:text-blue-gray-300 placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200 focus:border-2  focus:!border-blue-gray-300 focus:border-t-transparent focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50" 
                    icon={<FiSearch />}/>
            </div>
        </>
    );
}