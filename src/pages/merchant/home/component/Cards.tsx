import {
    Card,
    CardHeader,
    CardBody,
    Typography,
    Avatar,
  } from "@material-tailwind/react";
  import { useNavigate } from "react-router-dom";

  export function Cards() {
    const navigate = useNavigate();
    return (
      <Card
        shadow={false}
        className="relative grid h-[11rem] w-full max-w-[11rem] items-end justify-center overflow-hidden text-center mt-20 -mb-20"
        onClick={() => navigate("/client/sign-up")}
        >
        <CardHeader
          floated={false}
          shadow={false}
          color="transparent"
          className="absolute inset-0 m-0 h-full w-full rounded-none bg-[url('https://i.ibb.co/P6FyxmJ/image.png')] bg-cover bg-center"
        >
          <div className="to-bg-black-10 absolute inset-0 h-full w-full bg-gradient-to-t from-black/80 via-black/50" />
        </CardHeader>
        <CardBody className="m-auto py-14 px-6 md:px-12 place-self-center">
          <Typography
            variant="h4"
            color="white"
            className="font-medium leading-[1.5] relative"
          >
            asd
          </Typography>
          <div className="flex items-stretch ">
          <p className="text-white self-start absolute bottom-2 left-2 text-xs">asdads</p>
          </div>
        </CardBody>
      </Card>
    );
  }