import React from 'react';
import {
    Navbar, Collapse,
    Typography,
    IconButton,
    Avatar
} from '@material-tailwind/react'
import { Bars3Icon, XMarkIcon } from "@heroicons/react/24/outline";
import { FiMapPin } from "react-icons/fi";
import { useNavigate } from "react-router-dom";

function NavList() {
    return (
        <ul className="my-2 flex flex-col gap-2 lg:mb-0 lg:mt-0 lg:flex-row lg:items-center lg:gap-6">
            <Typography
                as="li"
                variant="small"
                color="blue-gray"
                className="p-1 font-medium"
            >
                <a href="#" className="flex items-center hover:text-blue-500 transition-colors">
                    Pages
                </a>
            </Typography>
            <Typography
                as="li"
                variant="small"
                color="blue-gray"
                className="p-1 font-medium"
            >
                <a href="#" className="flex items-center hover:text-blue-500 transition-colors">
                    Account
                </a>
            </Typography>
            <Typography
                as="li"
                variant="small"
                color="blue-gray"
                className="p-1 font-medium"
            >
                <a href="#" className="flex items-center hover:text-blue-500 transition-colors">
                    Blocks
                </a>
            </Typography>
            <Typography
                as="li"
                variant="small"
                color="blue-gray"
                className="p-1 font-medium"
            >
                <a href="#" className="flex items-center hover:text-blue-500 transition-colors">
                    Docs
                </a>
            </Typography>
        </ul>
    );
}
export const NavBar = () => {
    const navigate = useNavigate();
    const [openNav, setOpenNav] = React.useState(false);

    const handleWindowResize = () =>
        window.innerWidth >= 960 && setOpenNav(false);

    React.useEffect(() => {
        window.addEventListener("resize", handleWindowResize);
        return () => {
            window.removeEventListener("resize", handleWindowResize);
        };
    }, []);
    return (
        <>
            <Navbar className="px-6 py-3 bg-[#5DB329]">
                <div className="flex items-center justify-between">
                    <IconButton
                        variant="text"
                        className="h-6 w-6 text-inherit hover:bg-transparent focus:bg-transparent active:bg-transparent lg:hidden"
                        ripple={false}
                        onClick={() => setOpenNav(!openNav)}
                    >
                        {openNav ? (
                            <XMarkIcon className="h-6 w-6" strokeWidth={2} />
                        ) : (
                            <Bars3Icon className="h-6 w-6" strokeWidth={2} />
                        )}
                    </IconButton>
                    <FiMapPin className='ml-20 -mr-16 size-6'/>
                    <Typography
                        variant="h6"
                        className="m-auto py-1.5 text-white"
                    >
                        Bandung
                    </Typography>
                    <div className="hidden lg:block">
                        <NavList />
                    </div>
                    <Avatar src="https://docs.material-tailwind.com/img/face-2.jpg" alt="avatar"
                    onClick={() => navigate("/merchant/profile")}
                    />

                </div>
                <Collapse open={openNav}>
                    <NavList />
                </Collapse>
            </Navbar>
        </>

    );
}