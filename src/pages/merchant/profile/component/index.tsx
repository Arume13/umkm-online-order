import { Button, ButtonGroup, Input } from '@material-tailwind/react'
export const Index = () => {
    return (
        <>
            <img
                className="size-32 m-auto rounded-lg object-cover "
                src="https://images.unsplash.com/photo-1682407186023-12c70a4a35e0?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2832&q=80"
                alt="nature image"
            />

            <div className="mb-4 mt-10">
                <p className='font-semibold'>Nama Pengguna</p>
                <Input label="" className='mb-4'  />
            </div>
            <div className="mb-4">
                <p className='font-semibold'>Kata Sandi</p>
                <Input label="" className='mb-4'  />
            </div>
            <div className="mb-4">
                <p className='font-semibold'>Nomer Telefon</p>
                <Input label="" className='mb-4'  />
            </div>
            <div className="mb-4">
                <p className='font-semibold'>Deskripsi</p>
                <Input label="" className='mb-4 h-24'  />
            </div>
            <div className="flex mt-80 justify-center items-center">
                <div className="block w-[16rem]">
                    <Button size="lg" color='green' className='rounded-full' fullWidth>Simpan Perubahan</Button>
                </div>
            </div>
        </>
    );
}