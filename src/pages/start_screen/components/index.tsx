import { Button, ButtonGroup } from '@material-tailwind/react'
import { useNavigate } from "react-router-dom";

export const Index = () => {
    const navigate = useNavigate();

    return (
        <>
            <p className="text-xl font-bold text-gray-900 pt-16">
                Siap Untuk Mendukung
            </p>
            <p className="text-xl font-bold mb-16 lg:mb-10 text-gray-900">
                Kegiatan  anda hari ini
            </p>


            <svg width="439" height="518" viewBox="0 0 439 568" className='-mt-20 -ml-10 lg:-ml-10' fill="none" xmlns="http://www.w3.org/2000/svg">
                <g filter="url(#filter0_f_87_653)">
                    <ellipse cx="219" cy="259" rx="158" ry="159" fill="url(#paint0_linear_87_653)" />
                </g>
                <defs>
                    <filter id="filter0_f_87_653" x="-39" y="0" width="516" height="518" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                        <feFlood flood-opacity="0" result="BackgroundImageFix" />
                        <feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
                        <feGaussianBlur stdDeviation="50" result="effect1_foregroundBlur_87_653" />
                    </filter>
                    <linearGradient id="paint0_linear_87_653" x1="35.2391" y1="392.94" x2="424.216" y2="34.4785" gradientUnits="userSpaceOnUse">
                        <stop stop-color="#5DB329" />
                        <stop offset="0.825" stop-color="#D9D9D9" stop-opacity="0" />
                    </linearGradient>
                </defs>
            </svg>


            <div className="flex justify-center items-center ml-14 w-[12rem] -mt-[24rem] lg:w-[14rem] lg:-mt-[24rem]">
                <img src="https://i.ibb.co/nR7vKM0/image.png" className='' alt="" />
            </div>


            <p className="text-lg text-gray-900 mt-36 lg:mt-16">
                Selamat datang!
            </p>
            <p className="text-lg text-gray-900">
                Apakah anda Pedagang atau Pengguna?
            </p>


            <div className="flex mt-8 justify-center items-center">
                <ButtonGroup size="lg" color='green' fullWidth style={{ borderRadius: 100 }}>
                    <Button onClick={() => navigate("/merchant")}>Pedagang</Button>
                    <Button onClick={() => navigate("/client")}>Pengguna</Button>
                </ButtonGroup>
            </div>
        </>
    )
}
